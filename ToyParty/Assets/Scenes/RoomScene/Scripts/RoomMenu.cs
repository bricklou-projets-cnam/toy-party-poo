using UnityEngine;
using Mirror;
using TMPro;

[RequireComponent(typeof(Animator))]
public class RoomMenu : NetworkBehaviour
{
    [SerializeField] private GameObject playerList;
    [SerializeField] private GameObject playerListItemPrefab;
    [SerializeField] private TextMeshProUGUI readyButtonText;
    [SerializeField] private Color readyColor;
    [SerializeField] private Color notReadyColor;
    [SerializeField] private string readyText = "I am ready";
    [SerializeField] private string notReadyText = "I am not ready";
    private CustomNetworkRoomManager _networkRoomManager;

    private void Awake()
    {
        _networkRoomManager = FindObjectOfType<CustomNetworkRoomManager>();
    }

    private void OnEnable()
    {
        _networkRoomManager.OnRoomSlotsChanged += OnRoomSlotsChanged;
    }

    private void Start()
    {
        UpdatePlayerList();
        UpdateReadyButton(false);
    }

    private void OnDisable()
    {
        _networkRoomManager.OnRoomSlotsChanged -= OnRoomSlotsChanged;
    }

    public void OnRoomSlotsChanged()
    {
        UpdatePlayerList();
    }

    private void UpdatePlayerList()
    {
        foreach (Transform child in playerList.transform)
        {
            Destroy(child.gameObject);
        }

        foreach (CustomNetworkRoomPlayer player in _networkRoomManager.roomSlots)
        {
            if (player != null)
            {
                GameObject playerListItem = Instantiate(playerListItemPrefab, playerList.transform);
                playerListItem.GetComponent<PlayerListItem>().Setup(player);
            }
        }
    }

    public void ReturnToMainMenu()
    {
        Cursor.lockState = CursorLockMode.None;
        if (isClientOnly)
        {
            _networkRoomManager.StopClient();
        }
        else
        {
            _networkRoomManager.StopHost();
        }
    }

    public void ToggleReady()
    {
        CustomNetworkRoomPlayer localPlayer = NetworkClient.connection.identity.GetComponent<CustomNetworkRoomPlayer>();
        bool readyState = !localPlayer.readyToBegin;
        localPlayer.CmdChangeReadyState(readyState);
        UpdateReadyButton(readyState);
    }

    private void UpdateReadyButton(bool readyState)
    {
        readyButtonText.text = readyState ? notReadyText : readyText;
        readyButtonText.color = readyState ? notReadyColor : readyColor;
    }
}

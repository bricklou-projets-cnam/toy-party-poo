using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerListItem : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI playerNameText;
    [SerializeField] private TextMeshProUGUI playerStatusText;
    [SerializeField] private Color readyColor;
    [SerializeField] private Color notReadyColor;
    [SerializeField] private Color otherPlayerColor;
    [SerializeField] private Color localPlayerColor;
    private CustomNetworkRoomPlayer _player;
    private Image _image;

    private void Awake()
    {
        _image = GetComponent<Image>();
    }

    public void Setup(CustomNetworkRoomPlayer player)
    {
        RemoveEventListeners();
        _player = player;
        AddEventListeners();
        UpdateUI();
    }

    private void OnEnable() {
        AddEventListeners();
    }

    private void OnDisable()
    {
        RemoveEventListeners();
    }

    private void AddEventListeners()
    {
        if (!_player) return;
        _player.OnReadyStateChanged += _ => UpdateUI();
        _player.OnPlayerNameChanged += _ => UpdateUI();
    }
    private void RemoveEventListeners()
    {
        if (!_player) return;
        _player.OnReadyStateChanged -= _ => UpdateUI();
        _player.OnPlayerNameChanged -= _ => UpdateUI();
    }

    private void UpdateUI()
    {
        if (!_player || !playerNameText || !playerStatusText || !_image) return;
        playerNameText.text = GetPlayerNameText(_player);
        playerStatusText.text = GetPlayerStatusText(_player);
        playerStatusText.color = GetPlayerStatusColor(_player);
        _image.color = _player.isLocalPlayer ? localPlayerColor : otherPlayerColor;
    }

    private Color GetPlayerStatusColor(CustomNetworkRoomPlayer player)
    {
        return player.readyToBegin ? readyColor : notReadyColor;
    }

    private string GetPlayerStatusText(CustomNetworkRoomPlayer player)
    {
        return player.readyToBegin ? "Ready" : "Not Ready";
    }

    private string GetPlayerNameText(CustomNetworkRoomPlayer player)
    {
        return player.isLocalPlayer ? $"{player.PlayerName} (You)" : player.PlayerName;
    }
}

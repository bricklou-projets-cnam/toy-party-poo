using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Mirror;

public class GameManager : NetworkBehaviour
{
    public static GameManager Instance { get; private set; }
    public Dictionary<uint, Player> players = new Dictionary<uint, Player>();

    [SyncVar(hook = nameof(OnOrbOwnerChanged))]
    private Player _orbOwner;
    public Player OrbOwner => _orbOwner;
    private float orbSpeed = 3f;
    private GameObject orb;

    [SerializeField] private float initialGameTimer = 120f;

    [SyncVar(hook = nameof(OnGameTimerChanged))]
    private float gameTimer;
    public delegate void TimerCountDown(float remainingTime);
    public TimerCountDown OnTimerCountDown;
    public delegate void TimerEnded();
    public TimerEnded OnGameOver;
    public bool IsGameOver { get; private set;} = false;

    private void Awake()
    {
        gameTimer = initialGameTimer;
        Instance = this;
    }

    private void Start()
    {
        orb = GameObject.FindGameObjectWithTag("Orb");
    }

    public void RegisterPlayer(Player player)
    {
        players.Add(player.playerId, player);
    }

    public void UnregisterPlayer(Player player)
    {
        players.Remove(player.playerId);
    }

    public Player[] GetPlayersByScore()
    {
        return players.Values.OrderByDescending(player => player.Score).ToArray();
    }

    private void FixedUpdate()
    {
        if (IsGameOver) return;
        if (gameTimer <= 0f)
        {
            IsGameOver = true;
            OnGameOver?.Invoke();
            return;
        }

        if (isServer)
        {
            gameTimer = Mathf.Max(gameTimer - Time.fixedDeltaTime, 0f);
            _orbOwner?.AddScore(Time.fixedDeltaTime);
            RpcUpdateOrbPosition();
        }
    }

    private void OnGameTimerChanged(float oldValue, float newValue)
    {
        OnTimerCountDown?.Invoke(newValue);
    }

    [Server]
    public void SetOrbOwner(Player player)
    {
        _orbOwner = player;
    }

    [ClientRpc]
    private void RpcUpdateOrbPosition()
    {
        if (!_orbOwner) return;
        orb.transform.position = Vector3.Lerp(orb.transform.position, _orbOwner.playerHead.position + _orbOwner.orbOffset, Time.deltaTime * orbSpeed);
    }

    [Client]
    private void OnOrbOwnerChanged(Player oldOwner, Player newOwner)
    {
        oldOwner?.PlayOrbLostSound();
        newOwner?.PlayOrbPickupSound();
    }
}

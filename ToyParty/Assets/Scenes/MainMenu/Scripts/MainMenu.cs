using Mirror;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject networkManagerObject;
    private CustomNetworkDiscovery _networkDiscovery;

    private NetworkManager _networkManager;

    public void Start()
    {
        _networkManager = networkManagerObject.GetComponent<NetworkManager>();
        _networkDiscovery = networkManagerObject.GetComponent<CustomNetworkDiscovery>();
    }

    public void StartGame()
    {
        Debug.Log("Starting host...");
        _networkManager.StartHost();
        _networkDiscovery.AdvertiseServer();
    }

    public void DiscoverServers()
    {
        Debug.Log("Discovering servers...");
        _networkDiscovery.StartDiscovery();
    }

    public void JoinGame(DiscoveryResponse info)
    {
        Debug.Log("Found server: " + info.uri);
        _networkDiscovery.StopDiscovery();
        _networkManager.StartClient(info.uri);
    }

    public void CancelDiscovery()
    {
        Debug.Log("Cancelling discovery...");
        _networkDiscovery.StopDiscovery();
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
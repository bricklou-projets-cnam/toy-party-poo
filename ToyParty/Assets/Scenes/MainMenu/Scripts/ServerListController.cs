using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;

[Serializable]
public class OnServerClickedEvent : UnityEvent<DiscoveryResponse> {}

[RequireComponent(typeof(ScrollView))]
public class ServerListController : MonoBehaviour
{
    private readonly Dictionary<string, DiscoveryResponse> _discoveredServers = new();
    private readonly Dictionary<string, int> _serversIndex = new();
    private ScrollRect _scrollView;
    
    [SerializeField]
    private GameObject serverListElementPrefab;

    [SerializeField] private OnServerClickedEvent onServerClickedEvent;

    
    public void Start()
    {
        _scrollView = GetComponentInChildren<ScrollRect>();
    }
    
    public void OnDiscoveredServer(DiscoveryResponse response)
    {
        var uri = response.uri.ToString();
        if (!_discoveredServers.ContainsKey(response.uri.ToString()))
        {
            Debug.Log($"Found server: {uri}");
            _discoveredServers[uri] = response;

            var element = Instantiate(serverListElementPrefab, _scrollView.content.transform);
            _serversIndex[uri] = _scrollView.content.transform.childCount - 1;

            var textComponent = element.GetComponentInChildren<TextMeshProUGUI>();
            textComponent.text = response.ServerName;

            var buttonComponent = element.GetComponent<Button>();
            buttonComponent.onClick.AddListener(delegate { OnButtonClicked(response); });
        }
        else
        {
            var index = _serversIndex[uri];
            var element = _scrollView.content.transform.GetChild(index);
            
            var textComponent = element.GetComponentInChildren<TextMeshProUGUI>();
            textComponent.text = response.ServerName;
        }
    }

    private void OnButtonClicked(DiscoveryResponse response)
    {
        Debug.Log($"Loading server {response.uri}...");
        var server = _discoveredServers[response.uri.ToString()];
        onServerClickedEvent.Invoke(server);
    }
    
    public void CleanupDiscoveredServers()
    {
        _discoveredServers.Clear();
        foreach (Transform child in _scrollView.content.transform)
        {
            Destroy(child.gameObject);
        }
    }
}

using System.Collections.Generic;
using System.Linq;
using TMPro;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    [SerializeField] private AudioMixer audioMixer;

    [SerializeField] private PlayerPreferences _playerPreferences;

    [SerializeField] private TMP_Dropdown resolutionDropdown;

    [SerializeField] private UniversalRenderPipelineAsset[] _universalRenderPipelineAssets;

    private Resolution[] resolutions;

    // Start is called before the first frame update
    public void Start()
    {
        resolutions = Screen.resolutions.Select(resolution => new Resolution
        {
            width = resolution.width,
            height = resolution.height
        }).Distinct().ToArray();

        var options = new List<string>();

        var res = _playerPreferences.Resolution;
        if (res != null)
            Screen.SetResolution(
                res.Value.width,
                res.Value.height,
                _playerPreferences.Fullscreen
            );

        var currentResolutionIndex = 0;
        for (var i = 0; i < resolutions.Length; i++)
        {
            var option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.width &&
                resolutions[i].height == Screen.height)
                currentResolutionIndex = i;
        }

        resolutionDropdown.ClearOptions();
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();

        // Set the volume slider to the current volume
        GameObject.Find("MainVolumeSlider").GetComponent<Slider>().value = _playerPreferences.MainVolume;
        GameObject.Find("SfxVolumeSlider").GetComponent<Slider>().value = _playerPreferences.SfxVolume;
        GameObject.Find("MusicVolumeSlider").GetComponent<Slider>().value = _playerPreferences.MusicVolume;

        var qualitySlider = GameObject.Find("QualitySlider").GetComponent<Slider>(); 
        qualitySlider.value = _playerPreferences.Quality >= _universalRenderPipelineAssets.Length ? 0 : _playerPreferences.Quality;
        qualitySlider.maxValue = _universalRenderPipelineAssets.Length - 1;

        // Set the fullscreen toggle to the current fullscreen state
        GameObject.Find("FullscreenToggle").GetComponent<Toggle>().isOn = _playerPreferences.Fullscreen;
    }

    public void SetQuality(float numberF)
    {
        if (_universalRenderPipelineAssets.Length <= numberF || numberF < 0) return;

        var number = Mathf.RoundToInt(numberF);
        GraphicsSettings.renderPipelineAsset = _universalRenderPipelineAssets[number];
        _playerPreferences.Quality = number;
        
        Debug.Log("Default render pipeline asset is: " + GraphicsSettings.renderPipelineAsset.name);
    }

    public void SetMainVolume(float volume)
    {
        audioMixer.SetFloat("Volume", volume);
        _playerPreferences.MainVolume = volume;
    }
    
    public void SetMusicVolume(float volume)
    {
        audioMixer.SetFloat("MusicVolume", volume);
        _playerPreferences.MusicVolume = volume;
    }

    public void SetSfxVolume(float volume)
    {
        audioMixer.SetFloat("SfxVolume", volume);
        _playerPreferences.SfxVolume = volume;
    }

    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
        _playerPreferences.Fullscreen = isFullscreen;
    }

    public void SetResolution(int resolutionIndex)
    {
        var resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
        _playerPreferences.Resolution = resolution;
    }

    public void SaveAll()
    {
        _playerPreferences.Save();
    }
}
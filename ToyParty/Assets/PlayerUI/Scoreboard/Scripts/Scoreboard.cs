using UnityEngine;
using TMPro;

[RequireComponent(typeof(Animator))]
public class Scoreboard : MonoBehaviour
{
    [SerializeField] private GameObject scoreboardItemPrefab;
    [SerializeField] private Transform scoreboardList;
    [SerializeField] private GameObject buttons;
    [SerializeField] private TextMeshProUGUI mainPanelLabel;
    private Animator _animator;
    private static readonly int IsOpen = Animator.StringToHash("IsOpen");
    private Player _player;


    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _player = GetComponentInParent<Player>();
    }

    private void OnEnable()
    {
        GameManager.Instance.OnTimerCountDown += UpdateScoreboard;
        GameManager.Instance.OnGameOver += ShowButtons;
        GameManager.Instance.OnGameOver += SetGameOverLabel;
    }

    private void OnDisable()
    {
        GameManager.Instance.OnTimerCountDown -= UpdateScoreboard;
        GameManager.Instance.OnGameOver -= ShowButtons;
        GameManager.Instance.OnGameOver -= SetGameOverLabel;
    }

    private void SetGameOverLabel()
    {
        if(!mainPanelLabel) return;
        mainPanelLabel.text = "Game Over!";
        if(_player.isClientOnly)
        {
            mainPanelLabel.text += " Waiting for host to return to room...";
        }
    }

    private void CreateScoreboardItem(Player player, int rank)
    {
        GameObject scoreboardItemGO = (GameObject)Instantiate(scoreboardItemPrefab, scoreboardList);
        scoreboardItemGO?.GetComponent<ScoreboardItem>()?.Setup(player, rank);
    }

    public void ClearScoreboard()
    {
        foreach (Transform child in scoreboardList)
        {
            Destroy(child.gameObject);
        }
    }

    public void UpdateScoreboard(float remainingTime)
    {
        if(!scoreboardList) return;
        ClearScoreboard();
        Player[] players = GameManager.Instance.GetPlayersByScore();
        for (int i = 0; i < players.Length; i++)
        {
            CreateScoreboardItem(players[i], i + 1);
        }
    }

    public void Open()
    {
        _animator.SetBool(IsOpen, true);
    }

    public void Close()
    {
        _animator.SetBool(IsOpen, false);
    }

    public void ShowButtons()
    {
        if(!buttons) return;
        buttons.SetActive(true);

        if (_player.isClientOnly)
        {
            GameObject returnToRoomButton = buttons.transform.GetChild(0).gameObject;
            returnToRoomButton.SetActive(false);
        }
    }
}

using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ScoreboardItem : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI playerNameText;
    [SerializeField] private TextMeshProUGUI playerScoreText;
    [SerializeField] private TextMeshProUGUI playerRankText;
    [SerializeField] private Color otherPlayerColor;
    [SerializeField] private Color localPlayerColor;
    private Image _image;

    private void Awake()
    {
        _image = GetComponent<Image>();
    }

    public void Setup(Player player, int rank)
    {
        playerNameText.text = GetPlayerNameText(player);
        playerScoreText.text = GetScoreText(player);
        playerRankText.text = GetRankText(rank);
        _image.color = player.isLocalPlayer ? localPlayerColor : otherPlayerColor;
    }

    private string GetPlayerNameText(Player player)
    {
        return player.isLocalPlayer ? $"{player.PlayerName} (You)" : player.PlayerName;
    }

    private string GetScoreText(Player player)
    {
        return $"{player.Score:0} pts";
    }

    private string GetRankText(int rank)
    {
        switch (rank)
        {
            case 1:
                return "1st";
            case 2:
                return "2nd";
            case 3:
                return "3rd";
            default:
                return $"{rank}th";
        }
    }
}

using UnityEngine;
using UnityEngine.InputSystem;
using Mirror;
using System;

[RequireComponent(typeof(PlayerInput))]
public class PlayerUI : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenuObject;
    [SerializeField] private GameObject scoreboardObject;

    [SerializeField] private Player player;

    private PlayerInput _playerInput;
    private PauseMenu _pauseMenu;
    private NetworkRoomManager _networkRoomManager;
    private Scoreboard _scoreboard;

    public void Awake()
    {
        _pauseMenu = pauseMenuObject.GetComponent<PauseMenu>();
        _scoreboard = scoreboardObject.GetComponent<Scoreboard>();
        _playerInput = GetComponent<PlayerInput>();
        _networkRoomManager = FindObjectOfType<NetworkRoomManager>();
    }

    private void Start()
    {
        _scoreboard.Close();
    }

    private void OnEnable() {
        GameManager.Instance.OnGameOver += ShowGameOverScoreboard;
    }

    private void OnDisable() {
        GameManager.Instance.OnGameOver -= ShowGameOverScoreboard;
    }

    private void ShowGameOverScoreboard()
    {
        Cursor.lockState = CursorLockMode.None;
        _playerInput.SwitchCurrentActionMap("UI");
        _pauseMenu.Resume();
        _scoreboard.Open();
    }

    public void OnPause()
    {
        Cursor.lockState = CursorLockMode.None;
        _playerInput.SwitchCurrentActionMap("UI");
        _scoreboard.Close();
        _pauseMenu.Pause();
    }

    public void OnResume()
    {
        if(GameManager.Instance.IsGameOver) return;
        Cursor.lockState = CursorLockMode.Locked;
        _playerInput.SwitchCurrentActionMap("Player");
        _pauseMenu.Resume();
    }

    public void ReturnToRoom()
    {
        Cursor.lockState = CursorLockMode.None;
        CmdReturnToRoom();
    }

    private void CmdReturnToRoom()
    {
        _networkRoomManager.ServerChangeScene(_networkRoomManager.RoomScene);
    }

    public void ReturnToMainMenu()
    {
        Cursor.lockState = CursorLockMode.None;
        if (player.isClientOnly)
        {
            _networkRoomManager.StopClient();
        }
        else
        {
            _networkRoomManager.StopHost();
        }
    }

    public void OnScoreboard(InputValue value)
    {
        if (value.isPressed)
        {
            _scoreboard.Open();
        }
        else
        {
            _scoreboard.Close();
        }
    }
}
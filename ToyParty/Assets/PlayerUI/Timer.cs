using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class Timer : MonoBehaviour
{
    private TextMeshProUGUI label;

    public void Start()
    {
        label = GetComponentInChildren<TextMeshProUGUI>();
    }

    private void OnEnable()
    {
        GameManager.Instance.OnTimerCountDown += UpdateLabel;
    }

    private void OnDisable()
    {
        GameManager.Instance.OnTimerCountDown -= UpdateLabel;
    }

    private string ParseTime(float time)
    {
        var minutes = Mathf.FloorToInt(time / 60f);
        var seconds = Mathf.FloorToInt(time % 60f);
        return $"{minutes:00}:{seconds:00}";
    }

    private void UpdateLabel(float remainingTime)
    {
        if (!label) return;
        label.text = ParseTime(remainingTime);
    }
}
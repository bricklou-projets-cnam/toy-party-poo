using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PauseMenu : MonoBehaviour
{
    private Animator _animator;
    private static readonly int IsPaused = Animator.StringToHash("IsPaused");

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }
    
    public void Pause()
    {
        _animator.SetBool(IsPaused, true);
    }
    
    public void Resume()
    {
        _animator.SetBool(IsPaused, false);
    }
}

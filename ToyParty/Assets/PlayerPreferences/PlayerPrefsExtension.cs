﻿using UnityEngine;

namespace Preferences
{
    public class PlayerPrefs : UnityEngine.PlayerPrefs
    {
        public static bool GetBool(string key)
        {
            var val = GetInt(key, 0);
            return val == 1;
        }

        public static void SetBool(string key, bool value)
        {
            SetInt(key, value ? 1 : 0);
        }
    }
}
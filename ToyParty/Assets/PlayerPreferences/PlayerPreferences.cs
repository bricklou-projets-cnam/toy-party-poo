using System.Linq;
using UnityEngine;
using PlayerPrefs = UnityEngine.PlayerPrefs;

public class PlayerPreferences : MonoBehaviour
{
	private static PlayerPreferences Instance;

	public void Awake()
	{
		if (Instance == null)
		{
			DontDestroyOnLoad(this);
			Instance = this;
		}
		else
		{
			Destroy(this);
		}
	}

	public int Quality
	{
		get => PlayerPrefs.GetInt("quality");
		set => PlayerPrefs.SetInt("quality", value);
	}

	public float MainVolume
	{
		get => PlayerPrefs.GetFloat("volume");
		set => PlayerPrefs.SetFloat("volume", value);
	}

	public float MusicVolume
	{
		get => PlayerPrefs.GetFloat("musicVolume");
		set => PlayerPrefs.SetFloat("musicVolume", value);
	}

	public float SfxVolume
	{
		get => PlayerPrefs.GetFloat("sfxVolume");
		set => PlayerPrefs.SetFloat("sfxVolume", value);
	}

	public Resolution? Resolution
	{
		get
		{
			var res = PlayerPrefs.GetString("resolution");
			if (!res.Contains("x")) return null;
			var resAxis = res.Split("x");

			var resolution = new Resolution
			{
				width = int.Parse(resAxis[0]),
				height = int.Parse(resAxis[1])
			};

			return resolution;
		}
		set
		{
			if (value == null) return;

			Resolution resolution = value.Value;
			if (!IsValidResolution(resolution)) return;
			var res = value.Value.width + "x" + value.Value.height;
			PlayerPrefs.SetString("resolution", res);
		}
	}

	private bool IsValidResolution(Resolution resolution)
	{
		return Screen.resolutions.Any(res => res.width == resolution.width && res.height == resolution.height);
	}

	public bool Fullscreen
	{
		get => Preferences.PlayerPrefs.GetBool("fullscreen");
		set => Preferences.PlayerPrefs.SetBool("fullscreen", value);
	}

	public void Save()
	{
		PlayerPrefs.Save();
	}

	public void Clear()
	{
		PlayerPrefs.DeleteAll();
	}
}

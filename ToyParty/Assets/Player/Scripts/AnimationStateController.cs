using UnityEngine;
using Mirror;

[RequireComponent(typeof(Animator), typeof(CharacterController))]
public class AnimationStateController : NetworkBehaviour
{
    private Animator animator;
    private CharacterController characterController;
    [SerializeField] private float animationBlendSpeed = 10f;

    public void Start()
    {
        animator = GetComponent<Animator>();
        characterController = GetComponent<CharacterController>();
    }

    [ClientCallback]
    public void Update()
    {
        if (!isLocalPlayer) return;
        var relativeVelocity = transform.InverseTransformDirection(characterController.velocity);
        relativeVelocity.x = Mathf.Lerp(animator.GetFloat("X_Velocity"), relativeVelocity.x, animationBlendSpeed * Time.deltaTime);
        relativeVelocity.z = Mathf.Lerp(animator.GetFloat("Z_Velocity"), relativeVelocity.z, animationBlendSpeed * Time.deltaTime);
        animator.SetFloat("X_Velocity", relativeVelocity.x);
        animator.SetFloat("Z_Velocity", relativeVelocity.z);
    }
}

using System;
using UnityEngine;
using UnityEngine.InputSystem;
using Mirror;

[RequireComponent(typeof(CharacterController)), RequireComponent(typeof(PlayerInput))]
public class PlayerMovementController : NetworkBehaviour
{
    [Header("Movement")]
    private CharacterController characterController;
    private Vector3 velocity;
    private float currentMoveSpeed = 2f;
    [SerializeField] private float walkSpeed = 2f;
    [SerializeField] private float sprintSpeed = 4f;
    [SerializeField] private float gravity = -9.81f;
    [SerializeField] private float jumpHeight = 3f;

    [Header("Input")]
    private Vector2 moveInput;
    
    public bool IsGrounded => characterController.isGrounded;
    public bool IsSprinting => Math.Abs(currentMoveSpeed - sprintSpeed) < 0.1f;

    [ClientCallback]
    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
    }

    [ClientCallback]
    private void FixedUpdate()
    {
        ApplyGravity();
        ApplyMoveInput();

        Move();
    }

    [ClientCallback]
    public void OnMove(InputValue value)
    {
        moveInput = value.Get<Vector2>();
    }

    [ClientCallback]
    public void OnSprint(InputValue value)
    {
        currentMoveSpeed = value.isPressed ? sprintSpeed : walkSpeed;
    }

    [ClientCallback]
    public void OnJump(InputValue value)
    {
        if (characterController.isGrounded)
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
    }

    [Client]
    private void Move()
    {
        characterController.Move(velocity * Time.deltaTime);
    }

    [Client]
    private void ApplyMoveInput()
    {
        Vector3 moveDirection = new Vector3(moveInput.x, 0f, moveInput.y);
        moveDirection = transform.TransformDirection(moveDirection);
        velocity.x = moveDirection.x * currentMoveSpeed;
        velocity.z = moveDirection.z * currentMoveSpeed;
    }

    [Client]
    private void ApplyGravity()
    {
        velocity.y += gravity * Time.deltaTime;
        if (characterController.isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }
    }
}

using UnityEngine;
using UnityEngine.InputSystem;
using Mirror;

[RequireComponent(typeof(CharacterController))]
public class PlayerCameraController : NetworkBehaviour
{
    [Header("Camera")]
    [SerializeField] private Transform playerHead;
    [SerializeField] private Vector3 headOffset;
    [SerializeField] private float cameraSensitivity = 1f;
    [SerializeField] private float lookXLimit = 45f;
    [SerializeField] private float smoothAmount = 20f;

    private Camera cam;
    private Vector2 lookInput;
    private Vector2 rotation = Vector2.zero;

    [Header("Field of View")]
    private CharacterController characterController;
    [SerializeField] private float baseFov = 60f;
    [SerializeField] private float fovMultiplier = 5f;
    [SerializeField] private float fovLerpSpeed = 5f;

    [ClientCallback]
    private void Start()
    {
        cam = GetComponentInChildren<Camera>();
        characterController = GetComponent<CharacterController>();
        rotation.y = cam.transform.eulerAngles.y;
    }

    [ClientCallback]
    private void Update()
    {
        AdjustCameraPosition();
        RotateCamera();
        AdjustFov();
    }

    [ClientCallback]
    public void OnLook(InputValue value)
    {
        lookInput = value.Get<Vector2>();
    }

    [Client]
    private void AdjustCameraPosition()
    {
        cam.transform.position = Vector3.Lerp(
            cam.transform.position,
            playerHead.position + playerHead.TransformDirection(
                headOffset
            ),
            smoothAmount * Time.deltaTime
        );
    }

    [Client]
    private void RotateCamera()
    {
        rotation.y += lookInput.x * cameraSensitivity;
        rotation.x -= lookInput.y * cameraSensitivity;
        rotation.x = Mathf.Clamp(rotation.x, -lookXLimit, lookXLimit);
        cam.transform.localRotation = Quaternion.Euler(rotation.x, 0, 0);
        transform.eulerAngles = new Vector2(0, rotation.y);
    }

    [Client]
    private void AdjustFov()
    {
        float forwardVelocity = Vector3.Dot(
            characterController.velocity,
            transform.forward
        );
        forwardVelocity = Mathf.Clamp(forwardVelocity, 0f, 10f);
        float targetFov = baseFov + forwardVelocity * fovMultiplier;
        cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, targetFov, fovLerpSpeed * Time.deltaTime);
    }
}
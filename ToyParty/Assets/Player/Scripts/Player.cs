using Mirror;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Player : NetworkBehaviour
{
    private static readonly int BaseColor = Shader.PropertyToID("_BaseColor");

    [SerializeField] private Behaviour[] componentsToEnable;
    public Transform playerHead;
    public Vector3 orbOffset;

    [SerializeField] private AudioClip orbPickupSound;

    [SerializeField] private AudioClip orbLostSound;

    private AudioSource audioSource;

    public uint playerId => netId;
    [field: SyncVar(hook = nameof(UpdatePlayerName))] public string PlayerName { get; private set; } = "Anonymous Player";
    [field: SyncVar] public float Score { get; private set; }

    private void Start()
    {
        GameManager.Instance.RegisterPlayer(this);
        Cursor.lockState = CursorLockMode.Locked;
        if (isLocalPlayer)
        {
            Camera.main.GetComponent<AudioListener>().enabled = false;
            EnableComponents();
        }
        if(isServer) {
            TargetSetPlayerName($"Player {connectionToClient.connectionId.ToString("X")}");
        }

        audioSource = GetComponent<AudioSource>();
    }
    
    private void OnDestroy()
    {
        GameManager.Instance.UnregisterPlayer(this);
        Cursor.lockState = CursorLockMode.None;
    }

    private void OnDisable() {
        if (isLocalPlayer) {
            Camera.main.GetComponent<AudioListener>().enabled = true;
        }
    }

    private void UpdatePlayerName(string oldPlayerName, string playerName)
    {
        PlayerName = playerName;
        transform.name = PlayerName + " (Player)";
    }

    [TargetRpc]
    public void TargetSetPlayerName(string playerName)
    {
        PlayerName = playerName;
    }

    [ClientCallback]
    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Orb")) return;

        CmdSetOrbOwner();
    }

    private void EnableComponents()
    {
        foreach (var t in componentsToEnable) t.enabled = true;
    }

    [Command]
    private void CmdSetOrbOwner()
    {
        if (GameManager.Instance.OrbOwner == this) return;
        GameManager.Instance.SetOrbOwner(this);
    }

    [ClientRpc]
    public void AddScore(float amount)
    {
        Score += amount;
    }

    [Client]
    public void PlayOrbPickupSound()
    {
        if (!isLocalPlayer) return;
        audioSource.PlayOneShot(orbPickupSound);
    }

    [Client]
    public void PlayOrbLostSound()
    {
        if (!isLocalPlayer) return;
        audioSource.PlayOneShot(orbLostSound);
    }
}
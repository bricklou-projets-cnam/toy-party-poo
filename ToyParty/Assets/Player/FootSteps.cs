using UnityEngine;

[RequireComponent(typeof(AudioSource), typeof(PlayerMovementController))]
public class FootSteps : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] clips;

    private AudioSource _audioSource;
    
    private PlayerMovementController _playerMovementController;
    public void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _playerMovementController = GetComponent<PlayerMovementController>();
    }

    private void Steps()
    {
        var clip = GetRandomClip();
        if (!_playerMovementController.IsGrounded) return;
        
        var newVolume = _playerMovementController.IsSprinting ? 0.5f : 0.25f;
        _audioSource.PlayOneShot(clip, newVolume);
    }
    
    private AudioClip GetRandomClip()
    {
        return clips[Random.Range(0, clips.Length)];
    }
}

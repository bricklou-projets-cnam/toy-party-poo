using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(AudioSource))]
public class MusicManager : MonoBehaviour
{
    [SerializeField] private AudioClip[] menuClips;
    [SerializeField] private AudioClip[] inGameClips;
    
    private AudioSource _audioSource;
    public static MusicManager Instance { get; private set; }
    
    private MusicState _musicState = MusicState.Menu;

    private Coroutine _playNextClipCoroutine = null;
    
    private enum MusicState
    {
        Menu,
        InGame
    }

    private void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(this);
            Instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }
        
        _audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        PlayMenuMusic();
        
        // Play the next clip when the current one ends
        _playNextClipCoroutine = StartCoroutine(PlayNextClip());
    }

    public void PlayGameMusic()
    {
        if (_audioSource.isPlaying)
        {
            _audioSource.Stop();
        }
        
        if (inGameClips.Length == 0) return;

        if (_playNextClipCoroutine != null)
        {
            StopCoroutine(_playNextClipCoroutine);
        }

        // Play one of the in-game clips at random which is not the current one
        AudioClip clip = null;
        _musicState = MusicState.InGame;
        
        while (clip == _audioSource.clip || !clip)
        {
            clip = inGameClips[Random.Range(0, inGameClips.Length)];
        }
        
        _audioSource.clip = clip;
        _audioSource.Play();
        
        StartCoroutine(PlayNextClip());
    }
    
    public void PlayMenuMusic()
    {
        if (_audioSource.isPlaying)
        {
            _audioSource.Stop();
        }

        if (menuClips.Length == 0) return;
        
        if (_playNextClipCoroutine != null)
        {
            StopCoroutine(_playNextClipCoroutine);
        }
        
        // Play one of the menu clips at random which is not the current one
        AudioClip clip = null;
        _musicState = MusicState.Menu;

        while (clip == _audioSource.clip || !clip)
        {
            clip = menuClips[Random.Range(0, menuClips.Length)];
        }
        
        _audioSource.clip = clip;
        _audioSource.Play();
        
        StartCoroutine(PlayNextClip());
    }
    
    private IEnumerator PlayNextClip()
    {
        while (true)
        {
            yield return new WaitUntil(() => !_audioSource.isPlaying);
            
            if (_musicState == MusicState.Menu)
            {
                PlayMenuMusic();
            }
            else
            {
                PlayGameMusic();
            }
        }
    }
}

Assets/
├── Audio
│   ├── MusicManager
│   ├── MusicManager.meta
│   ├── SFX
│   ├── SFX.meta
│   ├── Tracks
│   └── Tracks.meta
├── Audio.meta
├── MainMixer.mixer
├── MainMixer.mixer.meta
├── Networking
│   ├── Prefabs
│   ├── Prefabs.meta
│   ├── Scripts
│   └── Scripts.meta
├── Networking.meta
├── Packages
│   ├── BOXOPHOBIC
│   ├── BOXOPHOBIC.meta
│   ├── Indie Game Helper
│   ├── Indie Game Helper.meta
│   ├── Mirror
│   ├── Mirror.meta
│   ├── QuickOutline
│   ├── QuickOutline.meta
│   ├── TextMesh Pro
│   ├── TextMesh Pro.meta
│   ├── UnityTechnologies
│   └── UnityTechnologies.meta
├── Packages.meta
├── Player
│   ├── Animations
│   ├── Animations.meta
│   ├── PlayerPreferences
│   ├── PlayerPreferences.meta
│   ├── Scripts
│   └── Scripts.meta
├── Player.meta
├── Plugins
│   ├── ParrelSync
│   └── ParrelSync.meta
├── Plugins.meta
├── Scenes
│   ├── MainMenu
│   ├── MainMenu.meta
│   ├── RoomScene
│   ├── RoomScene.meta
│   ├── TestScene
│   └── TestScene.meta
├── Scenes.meta
├── ScriptTemplates
│   ├── 50-Mirror__Network Manager-NewNetworkManager.cs.txt
│   ├── 50-Mirror__Network Manager-NewNetworkManager.cs.txt.meta
│   ├── 51-Mirror__Network Authenticator-NewNetworkAuthenticator.cs.txt
│   ├── 51-Mirror__Network Authenticator-NewNetworkAuthenticator.cs.txt.meta
│   ├── 52-Mirror__Network Behaviour-NewNetworkBehaviour.cs.txt
│   ├── 52-Mirror__Network Behaviour-NewNetworkBehaviour.cs.txt.meta
│   ├── 53-Mirror__Custom Interest Management-CustomInterestManagement.cs.txt
│   ├── 53-Mirror__Custom Interest Management-CustomInterestManagement.cs.txt.meta
│   ├── 54-Mirror__Network Room Manager-NewNetworkRoomManager.cs.txt
│   ├── 54-Mirror__Network Room Manager-NewNetworkRoomManager.cs.txt.meta
│   ├── 55-Mirror__Network Room Player-NewNetworkRoomPlayer.cs.txt
│   ├── 55-Mirror__Network Room Player-NewNetworkRoomPlayer.cs.txt.meta
│   ├── 56-Mirror__Network Discovery-NewNetworkDiscovery.cs.txt
│   ├── 56-Mirror__Network Discovery-NewNetworkDiscovery.cs.txt.meta
│   ├── 57-Mirror__Network Transform-NewNetworkTransform.cs.txt
│   └── 57-Mirror__Network Transform-NewNetworkTransform.cs.txt.meta
├── ScriptTemplates.meta
├── Settings
│   ├── InputActions
│   ├── InputActions.meta
│   ├── InputSystem.inputsettings.asset
│   ├── InputSystem.inputsettings.asset.meta
│   ├── Lighting Settings.lighting
│   ├── Lighting Settings.lighting.meta
│   ├── URP-Balanced-Renderer.asset
│   ├── URP-Balanced-Renderer.asset.meta
│   ├── URP-Balanced.asset
│   ├── URP-Balanced.asset.meta
│   ├── URP-HighFidelity-Renderer.asset
│   ├── URP-HighFidelity-Renderer.asset.meta
│   ├── URP-HighFidelity.asset
│   ├── URP-HighFidelity.asset.meta
│   ├── URP-Performant-Renderer.asset
│   ├── URP-Performant-Renderer.asset.meta
│   ├── URP-Performant.asset
│   └── URP-Performant.asset.meta
├── Settings.meta
├── Skybox
│   ├── Materials
│   ├── Materials.meta
│   ├── Settings
│   ├── Settings.meta
│   ├── Textures
│   └── Textures.meta
├── Skybox.meta
├── UIRessources
│   ├── Crosshair
│   ├── Crosshair.meta
│   ├── Fonts
│   ├── Fonts.meta
│   ├── Textures
│   ├── Textures.meta
│   ├── UIScroll
│   └── UIScroll.meta
├── UIRessources.meta
├── UniversalRenderPipelineGlobalSettings.asset
└── UniversalRenderPipelineGlobalSettings.asset.meta

36 directories, 72 files

# Toy party

Welcome to Toy Party, a multiplayer game where the goal is to capture the orb and keep it as long as you can. Rush at the orb and steal it from other players. Can you be the one to hold the orb the longest?

## How to play
To capture the orb, simply run over it. It will follow the player who is holding it. Your goal is to hold onto the orb for as long as possible. Beware though, other players will try to steal the orb from you.

To steal the orb, rush at the orb (not the player) and collide with it. If successful, you will become the new holder of the orb.

Note that, as it is a multiplayer game, it will not really pause when you open the pause menu. Use this menu to resume the game or leave the game.

## Controls
- Move Forward: `↑` or `W`
- Move Backward: `↓` or `S`
- Move Left: `←` or `A`
- Move Right: `→` or `D`
- Move Camera: `Mouse`
- Sprint: `Shift`
- Jump: `Space`
- Show Scoreboard: `Tab`
- Pause Menu: `Esc`

## Installation
Download the latest release, extract the archive and run `ToyParty.exe`
Alternatively, you can clone the repository and open the project in Unity. Build the game and run it.

## Credits
Toy Party was developed by:
- [Bricklou](https://gitlab.com/Bricklou)
- [samymosa02](https://gitlab.com/samymosa02)
- [d.ay](https://gitlab.com/d.ay)

**Enjoy the game!**
